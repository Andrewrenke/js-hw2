// Відповіді на запитання:

// 1. Рядкові, числові, BigInt(великі числа), null та undefined - невизначені.
// 2. == - означає схожість за типом данних, а === - ідентичність перемінних.
// 3. Оператор - це певна команда, яка допомогає задавати потрібну поведінку перемінним.


let userName = prompt("What is your name?")
while (userName === null) {
    alert("Enter your name again")
    userName = prompt("What is your name?")
}
let userAge = +prompt("How old are you?")
while (isNaN(userAge)) {
    alert("Enter your age again")
    userAge = +prompt("How old are you?")
}
if (userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert("Welcome, " + userName)
    } else {
        alert("You are not allowed to visit this website")
    }
} else {
    alert("Welcome, " + userName)
}

